package main

import "fmt"

const (
	companyInfo    = "One of Brisbane's most unbelievable success stories"
	companyCulture = "Passionate about achieving and maintaining market dominance through engineering-focused thinking and exceptional product delivery"
	beTechStack    = "Golang microservices built using Golang"
	feTechStack    = "Vue.js"
	recruiterName  = "Harold Liu"
	recruiterEmail = "harold@blackroc.co"
	recruiterPhone = "0400 659 332"
)

var (
	mustHaveSkills = []string{"Commercial Golang development",
		"Experience building on highly trafficked web applications",
		"Understanding of multiple microservice design patterns",
		"An interest in building massively scalable and highly elastic web services",
	}

	bonusSkills = []string{"Experience working with containerization and container orchestration tools",
		"Exceptional pragmatism and strong beliefs that are held loosely",
	}

	candidateSkills = map[string]bool{"Commercial Golang development": true,
		"Experience building on highly trafficked web applications":                  true,
		"Understanding of multiple microservice design patterns":                     true,
		"An interest in building massively scalable and highly elastic web services": true,
		"Experience working with containerization and container orchestration tools": true,
		"Exceptional pragmatism and strong beliefs that are held loosely":            true,
	}
)

func main() {
	var rating int
	jobSkills := append(mustHaveSkills, bonusSkills...)
	for _, skill := range jobSkills {
		if candidateSkills[skill] {
			rating++
		}
	}

	if rating >= 4 {
		fmt.Printf("Contact %s on %s or %s ", recruiterName, recruiterPhone, recruiterEmail)
	} else {
		fmt.Println("Keep trying!")
	}
}
